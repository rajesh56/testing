# testing go project

This project is having go library for creating pinger image and network.

## Task 1

Please use the docker file located at ./deployments to create the devops/pinger:latest image.

```bash
docker build -f ./deployments/build/Dockerfile -t devops/pinger:latest
```

## Task 2

instead of creating docker image manually, we have created a CI pipeline to create the docker image and store it as a tar file as we dont have a registry to push it.

Note: Since there is no docker runtime the image is failing to create. If you run locally where docker is installed the image will be successfully created.

```
Execte the ./gitlab-ci.yml file.
```
## Task3

We need to create a pinger service netwrok to create 2 containers that can talk to each other. Please check the docker-compose file located at ./deployments/docker-compose.yaml. When you issue the docker-compose up -d, it will create a bridge network and 2 services that can tall to eachother.

```
docker-compose up -d

```
##Task4

This file is a readme file.

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
